package de.blu.core.command;

import com.google.inject.Inject;
import de.blu.core.Core;
import de.blu.core.localize.Localize;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Collection;
import java.util.HashSet;
import java.util.UUID;

public class FlyCommand implements CommandExecutor {

    @Getter
    private JavaPlugin javaPlugin;

    @Getter
    private Localize localize;

    @Getter
    private Collection<UUID> flyingPlayers = new HashSet<>();

    @Inject
    private FlyCommand(JavaPlugin javaPlugin, Localize localize) {
        this.javaPlugin = javaPlugin;
        this.localize = localize;

        String commandName = "fly";
        PluginCommand command = javaPlugin.getCommand(commandName);
        if (command == null) {
            throw new NullPointerException("missing Command in plugins.yml: /" + commandName);
        }

        command.setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 0) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(Core.NO_CONSOLE_COMMAND);
                return false;
            }

            Player player = (Player) sender;

            if (!player.hasPermission("system.fly")) {
                player.sendMessage(Core.NO_PERMISSION_MESSAGE);
                return false;
            }

            if (this.getFlyingPlayers().contains(player.getUniqueId())) {
                this.getFlyingPlayers().remove(player.getUniqueId());
                player.sendMessage(this.getLocalize().getMessage("command.fly.left"));
                player.setAllowFlight(false);
                player.setFlying(false);
            } else {
                this.getFlyingPlayers().add(player.getUniqueId());
                player.sendMessage(this.getLocalize().getMessage("command.fly.enter"));
                player.setAllowFlight(true);
                player.setFlying(true);
            }
            return true;
        }

        if (!sender.hasPermission("system.fly.other")) {
            sender.sendMessage(Core.NO_PERMISSION_MESSAGE);
            return false;
        }

        String targetPlayerName = args[0];
        Player target = Bukkit.getPlayer(targetPlayerName);
        if (target == null) {
            sender.sendMessage(this.getLocalize().getMessage("player.not_online", targetPlayerName));
            return false;
        }

        if (this.getFlyingPlayers().contains(target.getUniqueId())) {
            this.getFlyingPlayers().remove(target.getUniqueId());
            sender.sendMessage(this.getLocalize().getMessage("command.fly.left.other", target.getName()));
            target.sendMessage(this.getLocalize().getMessage("command.fly.left"));
            target.setAllowFlight(false);
            target.setFlying(false);
        } else {
            this.getFlyingPlayers().add(target.getUniqueId());
            sender.sendMessage(this.getLocalize().getMessage("command.fly.enter.other", target.getName()));
            target.sendMessage(this.getLocalize().getMessage("command.fly.enter"));
            target.setAllowFlight(true);
            target.setFlying(true);
        }

        return false;
    }
}
