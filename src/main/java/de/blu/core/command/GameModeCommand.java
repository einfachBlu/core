package de.blu.core.command;

import com.google.inject.Inject;
import de.blu.core.Core;
import de.blu.core.localize.Localize;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class GameModeCommand implements CommandExecutor {

    @Getter
    private JavaPlugin javaPlugin;

    @Getter
    private Localize localize;

    @Inject
    private GameModeCommand(JavaPlugin javaPlugin, Localize localize) {
        this.javaPlugin = javaPlugin;
        this.localize = localize;

        String commandName = "gamemode";
        PluginCommand command = javaPlugin.getCommand(commandName);
        if (command == null) {
            throw new NullPointerException("missing Command in plugins.yml: /" + commandName);
        }

        command.setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(Core.NO_CONSOLE_COMMAND);
            return false;
        }
        Player player = (Player) sender;

        if (!player.hasPermission("system.gamemode")) {
            player.sendMessage(Core.NO_PERMISSION_MESSAGE);
            return false;
        }

        Player target;
        GameMode targetGameMode;

        if (args.length == 0) {
            player.sendMessage(this.getLocalize().getMessage("command.gamemode.usage"));
            return false;
        }

        switch (args[0].toLowerCase()) {
            case "0":
            case "survival":
            case "s":
                targetGameMode = GameMode.SURVIVAL;
                break;
            case "1":
            case "creative":
            case "c":
                targetGameMode = GameMode.CREATIVE;
                break;
            case "2":
            case "adventure":
            case "a":
                targetGameMode = GameMode.ADVENTURE;
                break;
            case "3":
            case "spectator":
            case "spc":
                targetGameMode = GameMode.SPECTATOR;
                break;
            default:
                player.sendMessage(this.getLocalize().getMessage("command.gamemode.unknown_gamemode", args[0]));
                return false;
        }

        if (args.length >= 2) {
            target = Bukkit.getPlayer(args[1]);
            if (target == null) {
                player.sendMessage(this.getLocalize().getMessage("player.not_online", args[1]));
                return false;
            }

            if (!player.hasPermission("system.gamemode." + targetGameMode.getValue() + ".other")) {
                player.sendMessage(Core.NO_PERMISSION_MESSAGE);
                return false;
            }

            player.sendMessage(this.getLocalize().getMessage("command.gamemode.success.other", target.getName(), targetGameMode.name()));
        } else {
            target = player;

            if (!player.hasPermission("system.gamemode." + targetGameMode.getValue())) {
                player.sendMessage(Core.NO_PERMISSION_MESSAGE);
                return false;
            }

            player.sendMessage(this.getLocalize().getMessage("command.gamemode.success", targetGameMode.name()));
        }

        target.setGameMode(targetGameMode);
        return false;
    }
}
