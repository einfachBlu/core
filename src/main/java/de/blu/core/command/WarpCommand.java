package de.blu.core.command;

import com.google.inject.Inject;
import de.blu.core.Core;
import de.blu.core.localize.Localize;
import de.blu.core.repository.WarpRepository;
import de.blu.core.util.ItemBuilder;
import lombok.Getter;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Arrays;
import java.util.Map;

public class WarpCommand implements CommandExecutor, Listener {

    @Getter
    private JavaPlugin javaPlugin;

    @Getter
    private Localize localize;

    @Getter
    private WarpRepository warpRepository;

    @Getter
    private Inventory inventory;

    @Inject
    private WarpCommand(JavaPlugin javaPlugin, Localize localize, WarpRepository warpRepository) {
        this.javaPlugin = javaPlugin;
        this.localize = localize;
        this.warpRepository = warpRepository;

        String commandName = "warp";
        PluginCommand command = javaPlugin.getCommand(commandName);
        if (command == null) {
            throw new NullPointerException("missing Command in plugins.yml: /" + commandName);
        }

        command.setExecutor(this);
        javaPlugin.getServer().getPluginManager().registerEvents(this, javaPlugin);

        // Init GUI
        this.inventory = Bukkit.createInventory(null, 45, "Warps");

        new BukkitRunnable() {
            @Override
            public void run() {
                inventory.clear();

                for (Map.Entry<String, Location> entry : warpRepository.getCachedWarps().entrySet()) {
                    String warpName = entry.getKey();

                    ItemStack itemStack = new ItemBuilder(Material.PAPER)
                            .setName(ChatColor.GOLD + warpName)
                            .setLore("§7§oKlicken, zum Teleportieren")
                            .toItemStack();

                    inventory.addItem(itemStack);
                }
            }
        }.runTaskTimer(javaPlugin, 20, 20 * 15);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(Core.NO_CONSOLE_COMMAND);
            return false;
        }

        Player player = (Player) sender;
        player.openInventory(this.getInventory());
        return false;
    }

    @EventHandler
    public void onClickEvent(InventoryClickEvent e) {
        if (e.getClickedInventory() != this.getInventory()) {
            return;
        }

        // Handle GUI Clicking
        ItemStack itemStack = e.getCurrentItem();
        if (itemStack == null || itemStack.getType().equals(Material.AIR)) {
            return;
        }

        Player player = (Player) e.getWhoClicked();

        e.setCancelled(true);
        ItemMeta itemMeta = itemStack.getItemMeta();
        String warpName = itemMeta.getDisplayName().substring(2);

        Location location = this.getWarpRepository().getWarpLocation(warpName);
        if (location == null) {
            System.out.println("Warp not exist: " + warpName);
            System.out.println(this.getWarpRepository().getCachedWarps().keySet().stream().filter(warpName::equalsIgnoreCase).findFirst().orElse(null));
            return;
        }

        player.teleport(location);
        player.playSound(player.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, 10, 1);
    }
}
