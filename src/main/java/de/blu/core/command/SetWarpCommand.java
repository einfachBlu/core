package de.blu.core.command;

import com.google.inject.Inject;
import de.blu.core.Core;
import de.blu.core.localize.Localize;
import de.blu.core.repository.WarpRepository;
import lombok.Getter;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class SetWarpCommand implements CommandExecutor {

    @Getter
    private JavaPlugin javaPlugin;

    @Getter
    private Localize localize;

    @Getter
    private WarpRepository warpRepository;

    @Inject
    private SetWarpCommand(JavaPlugin javaPlugin, Localize localize, WarpRepository warpRepository) {
        this.javaPlugin = javaPlugin;
        this.localize = localize;
        this.warpRepository = warpRepository;

        String commandName = "setwarp";
        PluginCommand command = javaPlugin.getCommand(commandName);
        if (command == null) {
            throw new NullPointerException("missing Command in plugins.yml: /" + commandName);
        }

        command.setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(Core.NO_CONSOLE_COMMAND);
            return false;
        }
        Player player = (Player) sender;

        if (args.length == 0) {
            sender.sendMessage(this.getLocalize().getMessage("command.setwarp.usage"));
            return false;
        }

        String warpName = args[0];
        this.getWarpRepository().setWarpLocation(warpName, player.getLocation());

        sender.sendMessage(this.getLocalize().getMessage("command.setwarp.success", warpName));
        return false;
    }
}
