package de.blu.core.command;

import com.google.inject.Inject;
import de.blu.core.Core;
import de.blu.core.localize.Localize;
import de.blu.core.repository.SpawnRepository;
import lombok.Getter;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class SpawnCommand implements CommandExecutor {

    @Getter
    private JavaPlugin javaPlugin;

    @Getter
    private Localize localize;

    @Getter
    private SpawnRepository spawnRepository;

    @Inject
    private SpawnCommand(JavaPlugin javaPlugin, Localize localize, SpawnRepository spawnRepository) {
        this.javaPlugin = javaPlugin;
        this.localize = localize;
        this.spawnRepository = spawnRepository;

        String commandName = "spawn";
        PluginCommand command = javaPlugin.getCommand(commandName);
        if (command == null) {
            throw new NullPointerException("Missing command in plugins.yml: /" + commandName);
        }

        command.setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(Core.NO_CONSOLE_COMMAND);
            return false;
        }

        Player player = (Player) sender;
        Location location = this.getSpawnRepository().getSpawnLocation();
        if (location == null) {
            player.sendMessage(this.getLocalize().getMessage("command.spawn.failure"));
            return false;
        }

        player.setFallDistance(0);
        player.teleport(location);
        player.sendMessage(this.getLocalize().getMessage("command.spawn.success"));
        return true;
    }
}
