package de.blu.core.command;

import com.google.inject.Inject;
import de.blu.core.localize.Localize;
import de.blu.core.repository.WarpRepository;
import lombok.Getter;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.plugin.java.JavaPlugin;

public class DelWarpCommand implements CommandExecutor {

    @Getter
    private JavaPlugin javaPlugin;

    @Getter
    private Localize localize;

    @Getter
    private WarpRepository warpRepository;

    @Inject
    private DelWarpCommand(JavaPlugin javaPlugin, Localize localize, WarpRepository warpRepository) {
        this.javaPlugin = javaPlugin;
        this.localize = localize;
        this.warpRepository = warpRepository;

        String commandName = "delwarp";
        PluginCommand command = javaPlugin.getCommand(commandName);
        if (command == null) {
            throw new NullPointerException("missing Command in plugins.yml: /" + commandName);
        }

        command.setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 0) {
            sender.sendMessage(this.getLocalize().getMessage("command.delwarp.usage"));
            return false;
        }

        String warpName = args[0];
        if (!this.getWarpRepository().containsWarpLocation(warpName)) {
            sender.sendMessage(this.getLocalize().getMessage("command.warp.not_exist", warpName));
            return false;
        }

        this.getWarpRepository().deleteWarpLocation(warpName);

        sender.sendMessage(this.getLocalize().getMessage("command.delwarp.success", warpName));
        return false;
    }
}
