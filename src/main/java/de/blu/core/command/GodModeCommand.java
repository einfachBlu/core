package de.blu.core.command;

import com.google.inject.Inject;
import de.blu.core.Core;
import de.blu.core.localize.Localize;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Collection;
import java.util.HashSet;
import java.util.UUID;

public class GodModeCommand implements Listener, CommandExecutor {

    @Getter
    private JavaPlugin javaPlugin;

    @Getter
    private Localize localize;

    @Getter
    private Collection<UUID> godPlayers = new HashSet<>();

    @Inject
    private GodModeCommand(JavaPlugin javaPlugin, Localize localize) {
        this.javaPlugin = javaPlugin;
        this.localize = localize;

        String commandName = "god";
        PluginCommand command = javaPlugin.getCommand(commandName);
        if (command == null) {
            throw new NullPointerException("missing Command in plugins.yml: /" + commandName);
        }

        command.setExecutor(this);
        javaPlugin.getServer().getPluginManager().registerEvents(this, javaPlugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 0) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(Core.NO_CONSOLE_COMMAND);
                return false;
            }

            Player player = (Player) sender;

            if (!player.hasPermission("system.god")) {
                player.sendMessage(Core.NO_PERMISSION_MESSAGE);
                return false;
            }

            if (this.getGodPlayers().contains(player.getUniqueId())) {
                this.getGodPlayers().remove(player.getUniqueId());
                player.sendMessage(this.getLocalize().getMessage("command.god.left"));
            } else {
                this.getGodPlayers().add(player.getUniqueId());
                player.sendMessage(this.getLocalize().getMessage("command.god.enter"));
                player.setFoodLevel(20);
                player.setHealth(20);
            }
            return true;
        }

        if (!sender.hasPermission("system.god.other")) {
            sender.sendMessage(Core.NO_PERMISSION_MESSAGE);
            return false;
        }

        String targetPlayerName = args[0];
        Player target = Bukkit.getPlayer(targetPlayerName);
        if (target == null) {
            sender.sendMessage(this.getLocalize().getMessage("player.not_online", targetPlayerName));
            return false;
        }

        if (this.getGodPlayers().contains(target.getUniqueId())) {
            this.getGodPlayers().remove(target.getUniqueId());
            sender.sendMessage(this.getLocalize().getMessage("command.god.left.other", target.getName()));
            target.sendMessage(this.getLocalize().getMessage("command.god.left"));
        } else {
            this.getGodPlayers().add(target.getUniqueId());
            sender.sendMessage(this.getLocalize().getMessage("command.god.enter.other", target.getName()));
            target.sendMessage(this.getLocalize().getMessage("command.god.enter"));
            target.setFoodLevel(20);
            target.setHealth(20);
        }

        return false;
    }

    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        if (!(e.getEntity() instanceof Player)) {
            return;
        }

        Player player = (Player) e.getEntity();
        System.out.println("EntityDamageEvent called for player " + e.getEntity().getName());

        if (!this.getGodPlayers().contains(player.getUniqueId())) {
            return;
        }

        e.setCancelled(true);
    }

    @EventHandler
    public void onFeed(FoodLevelChangeEvent e) {
        Player player = (Player) e.getEntity();

        if (!this.getGodPlayers().contains(player.getUniqueId())) {
            return;
        }

        e.setCancelled(true);
    }
}
