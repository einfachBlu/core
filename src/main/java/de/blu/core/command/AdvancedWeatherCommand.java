package de.blu.core.command;

import com.google.inject.Inject;
import de.blu.core.Core;
import de.blu.core.localize.Localize;
import lombok.Getter;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class AdvancedWeatherCommand implements CommandExecutor {

    @Getter
    private JavaPlugin javaPlugin;

    @Getter
    private Localize localize;

    @Inject
    private AdvancedWeatherCommand(JavaPlugin javaPlugin, Localize localize) {
        this.javaPlugin = javaPlugin;
        this.localize = localize;

        String commandName = "advancedweather";
        PluginCommand command = javaPlugin.getCommand(commandName);
        if (command == null) {
            throw new NullPointerException("missing Command in plugins.yml: /" + commandName);
        }

        command.setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(Core.NO_CONSOLE_COMMAND);
            return false;
        }

        Player player = (Player) sender;
        World world = player.getWorld();

        switch (label.toLowerCase()) {
            case "sun":
                world.setThundering(false);
                world.setStorm(false);
                break;
            case "thunder":
                world.setThundering(true);
                world.setStorm(true);
                break;
            case "day":
                world.setTime(10000);
                break;
            case "night":
                world.setTime(22000);
                break;
            default:
                break;
        }

        return false;
    }
}
