package de.blu.core.command;

import com.google.inject.Inject;
import de.blu.core.Core;
import de.blu.core.localize.Localize;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class TeamChatCommand implements CommandExecutor {

    @Getter
    private JavaPlugin javaPlugin;

    @Getter
    private Localize localize;

    @Inject
    private TeamChatCommand(JavaPlugin javaPlugin, Localize localize) {
        this.javaPlugin = javaPlugin;
        this.localize = localize;

        String commandName = "teamchat";
        PluginCommand command = javaPlugin.getCommand(commandName);
        if (command == null) {
            throw new NullPointerException("Missing command in plugins.yml: /" + commandName);
        }

        command.setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(Core.NO_CONSOLE_COMMAND);
            return false;
        }

        Player player = (Player) sender;

        if (args.length == 0) {
            player.sendMessage(this.getLocalize().getMessage("command.teamchat.usage"));
            return false;
        }

        String message = String.join(" ", args);
        message = ChatColor.translateAlternateColorCodes('&', this.getJavaPlugin().getConfig().getString("format.teamchat")
                .replaceAll("%player%", player.getName())
                .replaceAll("%message%", message));

        for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
            if (!onlinePlayer.hasPermission("system.teamchat")) {
                continue;
            }

            onlinePlayer.sendMessage(message);
        }

        return true;
    }
}
