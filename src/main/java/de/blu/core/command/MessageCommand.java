package de.blu.core.command;

import com.google.inject.Inject;
import de.blu.core.Core;
import de.blu.core.localize.Localize;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class MessageCommand implements CommandExecutor {

    @Getter
    private JavaPlugin javaPlugin;

    @Getter
    private Localize localize;

    @Getter
    private Map<UUID, UUID> lastReceivedMessageTarget = new HashMap<>();

    @Inject
    private MessageCommand(JavaPlugin javaPlugin, Localize localize) {
        this.javaPlugin = javaPlugin;
        this.localize = localize;

        String commandName = "message";
        PluginCommand command = javaPlugin.getCommand(commandName);
        if (command == null) {
            throw new NullPointerException("Missing command in plugins.yml: /" + commandName);
        }

        command.setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(Core.NO_CONSOLE_COMMAND);
            return false;
        }

        Player player = (Player) sender;

        Player target;
        String message;
        switch (label.toLowerCase()) {
            case "r":
            case "reply":
                if (args.length == 0) {
                    player.sendMessage(this.getLocalize().getMessage("command.reply.usage"));
                    return false;
                }

                if (!this.getLastReceivedMessageTarget().containsKey(player.getUniqueId())) {
                    player.sendMessage(this.getLocalize().getMessage("command.reply.failure.no_target"));
                    return false;
                }

                target = Bukkit.getPlayer(this.getLastReceivedMessageTarget().get(player.getUniqueId()));
                if (target == null) {
                    player.sendMessage(this.getLocalize().getMessage("command.reply.failure.not_online"));
                    return false;
                }

                message = String.join(" ", args);
                this.sendMessage(player, target, message);
                break;
            case "message":
            case "msg":
                if (args.length <= 1) {
                    player.sendMessage(this.getLocalize().getMessage("command.message.usage"));
                    return false;
                }

                String targetPlayerName = args[0];
                target = Bukkit.getPlayer(targetPlayerName);
                if (target == null) {
                    player.sendMessage(this.getLocalize().getMessage("player.not_online", targetPlayerName));
                    return false;
                }

                if (target.getUniqueId().equals(player.getUniqueId())) {
                    player.sendMessage(this.getLocalize().getMessage("command.message.failure"));
                    return false;
                }

                message = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
                this.sendMessage(player, target, message);
                break;
            default:
                player.sendMessage(this.getLocalize().getMessage("command.reply.usage"));
                player.sendMessage(this.getLocalize().getMessage("command.message.usage"));
                break;
        }
        return true;
    }

    private void sendMessage(Player player, Player target, String message) {
        this.getLastReceivedMessageTarget().put(target.getUniqueId(), player.getUniqueId());

        String senderMessage = ChatColor.translateAlternateColorCodes('&', this.getJavaPlugin().getConfig().getString("format.msg.sender")
                .replaceAll("%sender%", player.getName())
                .replaceAll("%receiver%", target.getName())
                .replaceAll("%message%", message));
        String receiverMessage = ChatColor.translateAlternateColorCodes('&', this.getJavaPlugin().getConfig().getString("format.msg.receiver")
                .replaceAll("%sender%", player.getName())
                .replaceAll("%receiver%", target.getName())
                .replaceAll("%message%", message));

        if (!senderMessage.equalsIgnoreCase("")) {
            player.sendMessage(senderMessage);
        }
        if (!receiverMessage.equalsIgnoreCase("")) {
            target.sendMessage(receiverMessage);
        }
    }
}
