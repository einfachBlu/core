package de.blu.core;

import com.google.inject.Guice;
import com.google.inject.Injector;
import de.blu.core.localize.Localize;
import de.blu.core.module.ModuleSettings;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class Core extends JavaPlugin {

    public static final String NO_PERMISSION_MESSAGE = ChatColor.RED + "I'm sorry, but you do not have permission to perform this command. Please contact the server administrators if you believe that this is a mistake.";
    public static final String NO_CONSOLE_COMMAND = ChatColor.RED + "Command is only ingame executeable!";

    @Override
    public void onEnable() {
        Injector injector = Guice.createInjector(new ModuleSettings(this));

        // Init basic Configuration
        this.saveDefaultConfiguration();

        // Init Messages
        injector.getInstance(Localize.class).load(new File(this.getDataFolder(), "messages.properties"));

        // For reloading
        for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
            this.getServer().getPluginManager().callEvent(new PlayerJoinEvent(onlinePlayer, ""));
        }
    }

    private void saveDefaultConfiguration() {
        FileConfiguration config = this.getConfig();
        if (!config.contains("format.teamchat")) {
            config.set("format.teamchat", "&7[&3Team&7] &r%player% &7» &f%message%");
            this.saveConfig();
        }

        if (!config.contains("format.chat")) {
            config.set("format.chat", "%player% &7» &f%message%");
            this.saveConfig();
        }

        if (!config.contains("format.msg.receiver")) {
            config.set("format.msg.receiver", "&6%sender% &7-> &6Dir &7» &f%message%");
            this.saveConfig();
        }

        if (!config.contains("format.msg.sender")) {
            config.set("format.msg.sender", "&6Du &7-> &6%receiver% &7» &f%message%");
            this.saveConfig();
        }
    }
}
