package de.blu.core.feature;

import com.google.inject.Inject;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class JoinFullServerFeature implements Listener {

    @Inject
    private JoinFullServerFeature(JavaPlugin javaPlugin) {
        javaPlugin.getServer().getPluginManager().registerEvents(this, javaPlugin);
    }

    @EventHandler
    public void onLogin(PlayerLoginEvent e) {
        Player player = e.getPlayer();

        if (!player.hasPermission("system.joinfullserver")) {
            return;
        }

        e.setResult(PlayerLoginEvent.Result.ALLOWED);
    }
}
