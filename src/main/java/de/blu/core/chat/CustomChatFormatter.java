package de.blu.core.chat;

import com.google.inject.Inject;
import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.java.JavaPlugin;
import ru.tehkode.permissions.PermissionGroup;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class CustomChatFormatter implements Listener {

    @Getter
    private JavaPlugin javaPlugin;

    @Inject
    private CustomChatFormatter(JavaPlugin javaPlugin) {
        this.javaPlugin = javaPlugin;
        javaPlugin.getServer().getPluginManager().registerEvents(this, javaPlugin);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onChat(AsyncPlayerChatEvent e) {
        Player player = e.getPlayer();

        if (e.isCancelled()) {
            return;
        }

        PermissionUser user = PermissionsEx.getUser(player);
        PermissionGroup defaultPermissionGroup = PermissionsEx.getPermissionManager().getDefaultGroups(player.getWorld().getName()).get(0);
        String playerDisplayName = (user.getGroups().length == 0 ? defaultPermissionGroup.getPrefix() : user.getGroups()[0].getPrefix()) + player.getName();

        String format = ChatColor.translateAlternateColorCodes('&', this.getJavaPlugin().getConfig().getString("format.chat")
                .replaceAll("%player%", playerDisplayName)
                .replaceAll("%message%", e.getMessage()));

        e.setFormat(format);
    }
}
