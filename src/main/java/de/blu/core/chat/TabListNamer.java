package de.blu.core.chat;

import com.google.inject.Inject;
import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import ru.tehkode.permissions.PermissionGroup;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class TabListNamer implements Listener {

    @Getter
    private JavaPlugin javaPlugin;

    @Inject
    private TabListNamer(JavaPlugin javaPlugin) {
        this.javaPlugin = javaPlugin;
        javaPlugin.getServer().getPluginManager().registerEvents(this, javaPlugin);
    }

    @EventHandler
    public void onChat(PlayerJoinEvent e) {
        Player player = e.getPlayer();

        PermissionUser user = PermissionsEx.getUser(player);
        PermissionGroup defaultPermissionGroup = PermissionsEx.getPermissionManager().getDefaultGroups(player.getWorld().getName()).get(0);
        String playerDisplayName = (user.getGroups().length == 0 ? defaultPermissionGroup.getPrefix() : user.getGroups()[0].getPrefix()) + player.getName();
        player.setPlayerListName(ChatColor.translateAlternateColorCodes('&', playerDisplayName));
    }
}
