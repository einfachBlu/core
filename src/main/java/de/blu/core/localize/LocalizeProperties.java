package de.blu.core.localize;

import com.google.inject.Inject;
import lombok.AccessLevel;
import lombok.Getter;
import org.bukkit.ChatColor;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class LocalizeProperties implements Localize {

    @Getter(AccessLevel.PRIVATE)
    private Map<String, String> messages = new HashMap<String, String>() {{
        this.put("prefix", "§7[§3GrandeCraft§7]§r");
        this.put("player.not_online", "%prefix% §cDer Spieler {0} ist nicht online.");
        this.put("command.god.enter", "%prefix% §aDu hast den GodMode betreten.");
        this.put("command.god.enter.other", "%prefix% §a{0} ist nun im GodMode.");
        this.put("command.god.left", "%prefix% §cDu hast den GodMode verlassen.");
        this.put("command.god.left.other", "%prefix% §c{0} ist nun nicht mehr im GodMode.");
        this.put("command.fly.enter", "%prefix% §aDu hast den FlyMode betreten.");
        this.put("command.fly.enter.other", "%prefix% §a{0} ist nun im FlyMode.");
        this.put("command.fly.left", "%prefix% §cDu hast den FlyMode verlassen.");
        this.put("command.fly.left.other", "%prefix% §c{0} ist nun nicht mehr im FlyMode.");
        this.put("command.setspawn.success", "%prefix% §aDu hast den Spawn auf deine aktuelle Position gesetzt.");
        this.put("command.spawn.failure", "%prefix% §cEs wurde keine Spawn Location gesetzt. Nutze /setspawn dafür.");
        this.put("command.spawn.success", "%prefix% §aDu wurdest zum Spawn teleportiert.");
        this.put("command.teamchat.usage", "%prefix% §eBenutzung: /teamchat <message>");
        this.put("command.reply.usage", "%prefix% §eBenutzung: /reply <message>");
        this.put("command.reply.failure.no_target", "%prefix% §cDir hat keiner geschrieben.");
        this.put("command.reply.failure.not_online", "%prefix% §cDer Spieler, welcher dir geschrieben hat, ist nicht mehr online.");
        this.put("command.message.usage", "%prefix% §eBenutzung: /msg <player> <message>");
        this.put("command.message.failure", "%prefix% §cDu kannst nicht mit dir selbst schreiben!");
        this.put("command.gamemode.usage", "%prefix% §eBenutzung: /gamemode <gamemode> [player]");
        this.put("command.gamemode.unknown_gamemode", "%prefix% §cDer GameMode {0} existiert nicht.");
        this.put("command.gamemode.success", "%prefix% §aDu bist nun im GameMode {0}.");
        this.put("command.gamemode.success.other", "%prefix% §aDu hast {0} in den GameMode {1} gesetzt.");
        this.put("command.setwarp.usage", "%prefix% §eBenutzung: /setwarp <name>");
        this.put("command.setwarp.success", "%prefix% §aDu hast die Position von dem Warp {0} gesetzt.");
        this.put("command.delwarp.usage", "%prefix% §eBenutzung: /delwarp <name>");
        this.put("command.delwarp.success", "%prefix% §cDu hast den Warp {0} entfernt.");
        this.put("command.warp.not_exist", "%prefix% §cDer Warp {0} existiert nicht.");
    }};

    @Inject
    private LocalizeProperties() {
    }

    @Override
    public void load(File file) {
        // Preload Messages from file
        Properties properties = new Properties();

        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }

        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            properties.load(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));

            boolean save = false;
            for (Map.Entry<String, String> entry : this.getMessages().entrySet()) {
                if (!properties.contains(entry.getKey())) {
                    properties.setProperty(entry.getKey(), entry.getValue());
                    save = true;
                }
            }

            if (save) {
                properties.store(new FileWriter(file), "Messages are all changeable");
            }

            for (Object key : properties.keySet()) {
                this.getMessages().put((String) key, properties.getProperty((String) key));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getMessage(String key, Object... args) {
        String message = this.getMessages().getOrDefault(key, "Localize: " + key + "");

        // Replace varargs
        for (int i = 0; i < args.length; i++) {
            message = message.replaceAll("\\{" + i + "}", String.valueOf(args[i]));
        }

        message = message.replaceAll("\\{NEXT_LINE}", "\n");

        // Replace custom locale Strings
        int count = 0;
        while (message.contains("%")) {
            boolean found = false;
            StringBuilder parameter = new StringBuilder();
            for (int i = 0; i < message.length(); i++) {
                Character character = message.charAt(i);
                if (character.equals("%".toCharArray()[0])) {
                    if (found) {
                        break;
                    }

                    found = true;
                    continue;
                }

                if (found) {
                    parameter.append(character.toString());
                }
            }

            message = message.replaceAll("%" + parameter + "%", this.getMessage(parameter.toString()));
            count++;

            if (count > 20) {
                break;
            }
        }

        return ChatColor.translateAlternateColorCodes('&', message);
    }

    @Override
    public boolean containsKey(String key) {
        return this.getMessages().containsKey(key);
    }
}
