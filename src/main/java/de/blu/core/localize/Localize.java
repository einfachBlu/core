package de.blu.core.localize;

import java.io.File;

public interface Localize {

    /**
     * Load Messages from a File
     * if no message file exist, then
     * it will load defaults from resources
     *
     * @param file the File where the Messages are stored
     */
    void load(File file);

    /**
     * Get a message from the given key & args
     *
     * @param key  the key from the message
     * @param args the args to replace
     * @return the message
     */
    String getMessage(String key, Object... args);

    /**
     * Check if a key exist
     *
     * @param key the searched key
     * @return true if the key was found otherwise false
     */
    boolean containsKey(String key);
}
