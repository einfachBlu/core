package de.blu.core.location;

import org.bukkit.Location;

import java.util.Map;

public interface LocationSerializer {

    /**
     * Save Location to the given locationKey
     *
     * @param locationKey the key under which the location is reachable
     * @param location    the location
     */
    void saveLocation(String locationKey, Location location);

    /**
     * Get a Location by the locationKey
     *
     * @param locationKey the locationKey
     * @return Location or null if no location was saved under this key
     */
    Location getLocation(String locationKey);

    /**
     * Check if a location exist for a key
     *
     * @param locationKey the Key to check
     * @return true if the location exist, otherwise false
     */
    boolean containsLocation(String locationKey);

    /**
     * Get all Locations in the Storage
     *
     * @return Map with all LocationKeys and their Locations
     */
    Map<String, Location> getLocations();

    /**
     * Remove a specified Location from the Storage
     *
     * @param locationKey the locationKey to remove
     */
    void removeLocation(String locationKey);
}
