package de.blu.core.location;

import com.google.inject.Inject;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ConfigLocationSerializer implements LocationSerializer {

    @Getter
    private JavaPlugin javaPlugin;

    @Getter
    private YamlConfiguration config;

    @Getter
    private File configFile;

    @Getter
    private Map<String, Location> cachedLocations = new HashMap<>();

    @Inject
    private ConfigLocationSerializer(JavaPlugin javaPlugin) {
        this.javaPlugin = javaPlugin;

        this.configFile = new File(javaPlugin.getDataFolder(), "locations.yml");
        if (!this.getConfigFile().exists()) {
            try {
                this.getConfigFile().getParentFile().mkdirs();
                this.getConfigFile().createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        this.config = YamlConfiguration.loadConfiguration(this.getConfigFile());
        this.loadLocations();
    }

    @Override
    public void saveLocation(String configKey, Location location) {
        this.getCachedLocations().put(configKey, location);

        this.getConfig().set(configKey + ".location.world", Objects.requireNonNull(location.getWorld()).getName());
        this.getConfig().set(configKey + ".location.x", location.getX());
        this.getConfig().set(configKey + ".location.y", location.getY());
        this.getConfig().set(configKey + ".location.z", location.getZ());
        this.getConfig().set(configKey + ".location.yaw", location.getYaw());
        this.getConfig().set(configKey + ".location.pitch", location.getPitch());

        try {
            this.getConfig().save(this.getConfigFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Location getLocation(String configKey) {
        return this.getCachedLocations().get(configKey);
    }

    @Override
    public boolean containsLocation(String configKey) {
        return this.getCachedLocations().containsKey(configKey);
    }

    private void loadLocations() {
        try {
            this.getConfig().load(this.getConfigFile());
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }

        for (String key : this.getConfig().getKeys(false)) {
            String locationKey = key.replaceAll(".location.world", "");
            String worldName = this.getConfig().getString(locationKey + ".location.world");
            double x = this.getConfig().getDouble(locationKey + ".location.x");
            double y = this.getConfig().getDouble(locationKey + ".location.y");
            double z = this.getConfig().getDouble(locationKey + ".location.z");
            float yaw = (float) this.getConfig().getDouble(locationKey + ".location.yaw");
            float pitch = (float) this.getConfig().getDouble(locationKey + ".location.pitch");
            assert worldName != null;

            World world = Bukkit.getWorld(worldName);
            if (world == null) {
                world = Bukkit.createWorld(new WorldCreator(worldName));
            }

            this.getCachedLocations().put(locationKey, new Location(world, x, y, z, yaw, pitch));
        }
    }

    @Override
    public Map<String, Location> getLocations() {
        return this.getCachedLocations();
    }

    @Override
    public void removeLocation(String locationKey) {
        if (!this.containsLocation(locationKey)) {
            return;
        }

        this.getCachedLocations().remove(locationKey);
        this.getConfig().set(locationKey, null);
        try {
            this.getConfig().save(this.getConfigFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
