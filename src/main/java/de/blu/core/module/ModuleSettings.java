package de.blu.core.module;

import com.google.inject.AbstractModule;
import de.blu.core.Core;
import de.blu.core.localize.Localize;
import de.blu.core.localize.LocalizeProperties;
import de.blu.core.location.ConfigLocationSerializer;
import de.blu.core.location.LocationSerializer;
import de.blu.core.repository.SpawnRepository;
import de.blu.core.repository.WarpRepository;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.TabExecutor;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.reflections.Reflections;

import java.util.Set;
import java.util.stream.Collectors;

public class ModuleSettings extends AbstractModule {

    @Getter
    private Core core;

    @Getter
    private String basePackageName = "de.blu.core";

    public ModuleSettings(Core core) {
        this.core = core;
    }

    @Override
    protected void configure() {
        // Binding Bukkit Things
        bind(JavaPlugin.class).toInstance(this.getCore());
        bind(Plugin.class).toInstance(this.getCore());
        bind(Server.class).toInstance(Bukkit.getServer());

        // Bind Other
        bind(Localize.class).to(LocalizeProperties.class).asEagerSingleton();
        bind(SpawnRepository.class).asEagerSingleton();
        bind(WarpRepository.class).asEagerSingleton();
        bind(LocationSerializer.class).to(ConfigLocationSerializer.class).asEagerSingleton();

        // Bind Recursive Commands & Listener
        this.bindListeners();
        this.bindCommands();
    }

    private void bindListeners() {
        Reflections reflections = new Reflections(this.getBasePackageName());
        Set<Class<? extends Listener>> allClasses = reflections.getSubTypesOf(Listener.class).stream().filter(aClass -> aClass.getName().startsWith(this.getBasePackageName())).collect(Collectors.toSet());

        for (Class<? extends Listener> listenerClass : allClasses) {
            bind(listenerClass).asEagerSingleton();
            this.getCore().getServer().getConsoleSender().sendMessage("§aRegistered Listener §6" + listenerClass.getName());
        }

        this.getCore().getServer().getConsoleSender().sendMessage("§aRegistered §6" + allClasses.size() + " §aListeners");
    }

    private void bindCommands() {
        Reflections reflections = new Reflections(this.getBasePackageName());
        Set<Class<? extends CommandExecutor>> allClasses = reflections.getSubTypesOf(CommandExecutor.class).stream().filter(aClass -> aClass.getName().startsWith(this.getBasePackageName())).collect(Collectors.toSet());
        Set<Class<? extends TabExecutor>> excludedClasses = reflections.getSubTypesOf(TabExecutor.class);

        int registeredAmount = 0;
        for (Class<? extends CommandExecutor> commandExecutorClass : allClasses) {
            if (excludedClasses.contains(commandExecutorClass)) {
                continue;
            }

            bind(commandExecutorClass).asEagerSingleton();
            this.getCore().getServer().getConsoleSender().sendMessage("§aRegistered Command §6" + commandExecutorClass.getName());
            registeredAmount++;
        }

        this.getCore().getServer().getConsoleSender().sendMessage("§aRegistered §6" + registeredAmount + " §aCommands");
    }
}
