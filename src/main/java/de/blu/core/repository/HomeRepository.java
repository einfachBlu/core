package de.blu.core.repository;

import com.google.inject.Inject;
import de.blu.core.location.LocationSerializer;
import lombok.Getter;
import org.bukkit.Location;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class HomeRepository {

    @Getter
    private JavaPlugin javaPlugin;

    @Getter
    private LocationSerializer locationSerializer;

    @Getter
    private Map<UUID, List<Location>> cachedHomeLocations = new HashMap<>();

    // Home Key Example: home_uuidhere_HOMENAME

    @Inject
    private HomeRepository(JavaPlugin javaPlugin, LocationSerializer locationSerializer) {
        this.javaPlugin = javaPlugin;
        this.locationSerializer = locationSerializer;
    }

    public void loadHomes(UUID player) {
    }

    /*
    public void setWarpLocation(String warpName, Location location) {
        this.getCachedWarps().put(warpName, location);

        this.getLocationSerializer().saveLocation("warp_" + warpName, location);
    }

    public void deleteWarpLocation(String warpName) {
        this.getCachedWarps().remove(warpName);
        this.getLocationSerializer().removeLocation("warp_" + warpName);
    }

    public Location getWarpLocation(String warpName) {
        return this.getCachedWarps().get(warpName);
    }

    public boolean containsWarpLocation(String warpName) {
        return this.getCachedWarps().containsKey(warpName);
    }

    private void loadWarps() {
        for (String locationKey : this.getLocationSerializer().getLocations().keySet()) {
            if (!locationKey.startsWith("warp")) {
                continue;
            }

            String warpName = locationKey.substring("warp_".length());
            this.getCachedWarps().put(warpName, this.getCachedWarps().get(locationKey));
        }
    }
     */
}
