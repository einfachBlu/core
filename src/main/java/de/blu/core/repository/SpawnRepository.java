package de.blu.core.repository;

import com.google.inject.Inject;
import de.blu.core.location.LocationSerializer;
import lombok.Getter;
import org.bukkit.Location;
import org.bukkit.plugin.java.JavaPlugin;

public class SpawnRepository {

    @Getter
    private JavaPlugin javaPlugin;

    @Getter
    private LocationSerializer locationSerializer;

    @Getter
    private Location cachedSpawnLocation;

    @Inject
    private SpawnRepository(JavaPlugin javaPlugin, LocationSerializer locationSerializer) {
        this.javaPlugin = javaPlugin;
        this.locationSerializer = locationSerializer;
    }

    public void setSpawnLocation(Location location) {
        this.cachedSpawnLocation = location;

        this.getLocationSerializer().saveLocation("spawn", location);
    }

    public Location getSpawnLocation() {
        if (this.getCachedSpawnLocation() == null) {
            this.loadSpawnLocation();
        }

        return this.getCachedSpawnLocation();
    }

    private void loadSpawnLocation() {
        this.cachedSpawnLocation = this.getLocationSerializer().getLocation("spawn");
    }
}
